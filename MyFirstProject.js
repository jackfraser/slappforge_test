import {SharedService } from './shared/shared.service';
const sharedService = new SharedService()
exports.handler = async (event) => {
    const output = sharedService.testThis(10,5);
    return {"message": "Successfully executed", output};
};